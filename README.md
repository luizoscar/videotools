# Video Files conversion tool

This is a Python application to easy the process to convert video files using the open source media conversion tool [ffmpeg](https://www.ffmpeg.org/).  
From a given directory, the application will search and display the list of video files with the respective codec informations.  
On the right side, there is a toolbar with the actions that can be applied to the selected files.  
This application is supposed to work with both Python 2.X & 3.X, and can be executed both on Linux and Windows, as long as the GTK3+ library is installed.  

**Important:**

In order to support some features like H265 HEVC, AOMedia AV1 Codec and video stabilization, sometimes it is necessary to recompile ffmpeg, enabling such functionality; This can be done following the [Official guide to compile FFmpeg for Ubuntu, Debian, or Mint](https://trac.ffmpeg.org/wiki/CompilationGuide) and the [vidstab library git repository](https://github.com/georgmartius/vid.stab).

## Supported formats

The application is able to convert between multiple formats, among then are: wmv, avi, mpg, 3gp, mov, m4v, mts, mp4 and webm.  

The list of supported codecs are mapped on the codecs.xml file
Currently mapped codecs are:

1. Video Codecs
    + AOMedia AV1 -Matroska (Container) + AV1 (Video) + Opus (Audio)  **__Slowest__**
    + H265 HEVC - MPEG (Container) + H265 (Video) + AAC (Audio)  **__Best compression__**
    + H264 AVC - MPEG (Container) + H264 (Video) + AAC (Audio)  **__Faster and most compatible__**
    + WebM VP8 - Matroska (Container) + VP8 (Video) + Vorbis (Audio)  
    + WebM VP9 - Matroska (Container) + VP9 (Video) + Opus (Audio)  

1. Audio Codecs
    + MP3  
    + AAC  
    + FLAC  
    + Ogg Vorbis  

**Note:**  
The list of supported codecs may vary according the available ffmpeg codecs.

## Application usage

The application main screen, where the user can specify the source directory, select the files and the transformation option to be applied to the selected files.  

![alt text](imagens/main_window.png "Application main screen")

After selecting the source directories, the user should click **Refresh**; The application will search for video files in the tree of the sub-directories, displaying the video informations in the file grid: file name, file size, codec details and a check box, so the user can select the file.  
After selecting the files that should be processed, the user must click a button from the toolbar to perform the operation.  
A pop-up menu is also available for easy the selection and deletion in the file grid.  

## Features

Following is the list of features available on the application.  

### Convert

It allows to convert the selected video files to a specified codec or export the audio track.  
When clicked, a screen will be displayed with the list of codecs supported by ffmpeg that is configured inside the application.  
Upon confirmation, the application will start the conversion process and display a screen, with the progress of the activity.  
The converted file will be generated in the home screen directory and will have a prefix with the name of the codec and the extension of the codec.  

**Note:**  
If you select one of the audio codecs, the application will only extract the audio track.  

![alt text](imagens/export_dialog.png "Video conversion screen")

### Resize

Allows you to change the resolution of the selected videos by reducing the width and height of the video file.  
A screen will appear asking for the new resolution, with the resolution of the first selected video filled in by default.  

![alt text](imagens/resize_dialog.png "Video resize screen")

### Rotate

Allows the rotation and flip of the selected videos.  

The following options are available:  

+ 90 degrees clockwise
+ 90 Degrees counterclockwise  
+ 180 Degrees  
+ 90 Degrees anti-clockwise with vertical flip  
+ 90 degrees clockwise with vertical flip  
+ Flip horizontal  
+ Vertical Flip  

![alt text](imagens/rotate_dialog.png "Video rotation screen")

### Extract Interval

This feature makes it possible to create a video from an interval of the selected video.  
A screen will be displayed with the start time and the end time of the block to be extracted, by default the final block will display the total size of the video.  

![alt text](imagens/extract_section_dialog.png "Interval extraction screen")

**Note:**  
This feature is only available if only one video is selected in the list.  

### Extract Region

It allows you to create a video with a region of the original video (crop).  
A screen with the initial position (vertical and horizontal) and the size (width and height) of the section to be captured will be displayed.  

![alt text](imagens/extract_region_dialog.png "Vídeo Region Extraction Screen")

**Note:**  
This feature is only available if only one video is selected in the list.  

### Concatenate

It allows you to create a video from a sequence of videos (concatenate videos).  
A screen will be displayed with the list of videos that will be concatenated and the codec of the destination video.  
The sequence of the videos will be the sequence of the files in the text box.  

![alt text](imagens/concatenate_dialog.png "Video concatenation screen")

**Note:**  
This feature is only available if more than one video is selected in the list.  

### Stabilization

Enables video stabilization (deshake).  
This is an experimental feature that allows the correction of videos without stabilization (shaking).  

**Note:**  
This functionality is only available if ffmpeg is compiled with the flag `--enable-libvidstab` check the details on the [VidStab Library](https://github.com/georgmartius/vid.stab) .  

## Application log and configuration file

### Settings file

The application settings are in the `settings.xml` file, the file has the following syntax:  

```xml
<?xml version='1.0' encoding='UTF-8'?>
<config>
    <video_extensions>wmv|avi|mpg|3gp|mov|m4v|mts|mp4|webm</video_extensions>
    <path_to_ffmpeg>ffmpeg</path_to_ffmpeg>
    <source_dir>/home/oscar/Desktop/Videos</source_dir>
    <window_x>228</window_x>
    <window_y>190</window_y>
    <window_w>1280</window_w>
    <window_h>574</window_h>
</config>
```

+ The `video_extensions` section defines the list of extensions that will be considered a video file by the application. The mapping of the extensions is case insensitive.  

+ The `path_to_ffmpeg` section indicates the path to the ffmpeg application. If this path is not specified, the application will try to use the ffmpeg on the system path.  

+ The `source_dir` section indicates the last directory used by the application.  

+ The `window_*` Indicates the applications latest size and position.

### System log

During execution, the application will generate a log in the `application.log` file. This file can be viewed by clicking on the `Logs` button on the home screen or by editing with any text editor.

## Prerequisites for running the application

For the complete operation of the application it is necessary that the following tools are installed in the system:  

1. Python `2.7` or `3.5`.
2. Python libraries: **LXML, future and GTK3**.
3. Ffmpeg media converter in system path or configured in `settings.xml` file.  

## Installation of Requirements

### Linux

1 - Install Python, PyPI and ffmpeg:

```sh
sudo apt-get install python-pip ffmpeg python-gi
```

2 - Using python-pip, install the `lxml` and `future` library:

```sh
sudo pip install lxml future
```

**Note:**  
In Linux (ubuntu) you will not need to install GTK3.  

### Windows

1 - Install [Python 2.7](https://www.python.org/downloads/release/python-2713/) or [Python 3.5](https://www.python.org/downloads/release/python-363/).

**Warning:**  
During the installation, remember to check the option to add Python to the System Path or manually add the PATH environment variable.  

+ Python Installation

![alt text](imagens/windows_python.png "Python 2.7 Installation")

2 - Install [PyGObject for Windows](https://sourceforge.net/projects/pygobjectwin32/files/pygi-aio-3.18.2_rev12-setup_549872deadabb77a91efbc56c50fe15f969e5681.exe/download)

**Warning:**  
During installation, the following components should be selected: `Base Packages` and `GTK + 3.18.9` (see images).  

+ Selecting the directory where Python 2.7 is installed

![alt text](imagens/pygobject1.png "Selecting the Python 2.7 directory")

+ Select the Python 2.7

**Note:**  
If the same version appears more than once, select only once.  

![alt text](imagens/pygobject2.png "Select the Python 2.7")

+ Selection of basic components

![alt text](imagens/pygobject3.png "Selection of basic components")

+ Selection of GTK3 +

![alt text](imagens/pygobject4.png "Selection of GTK3+")

+ Ignore non-GNU packages

![alt text](imagens/pygobject5.png "Ignore non-GNU packages")

+ Ignore additional packages

![alt text](imagens/pygobject6.png "Ignore additional packages")

+ Proceed with the installation

![alt text](imagens/pygobject7.png "Proceed with the installation")

3 - Using python pip via Command Line, install the `lxml` and `future` library:

```sh
pip install lxml future
```

4 - Install [ffmpeg](https://www.ffmpeg.org/download.html) and add it to the system path or configure it in `settings.xml` file.
