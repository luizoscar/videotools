# -*- coding: utf-8 -*-

'''
###############################################################################################'
 InputComboBoxDialog.py

 This is a free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This file is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with the software; see the file COPYING. If not, write to the
 Free Software Foundation, Inc.,
 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.

###############################################################################################'
 @class Dialog used request information from the user using a combo box 
 @author: Luiz Oscar Machado Barbosa - <luizoscar@gmail.com>
##############################################################################################
'''
from gi.repository import Gtk

from modules.dialogs.BaseContainer import BaseDialog


class InputComboBoxDialog(BaseDialog):
    """
    Input dialog using Combo Box
    """

    def __init__(self, parent, msg, option_list, default=None):
        '''
        Default constructor
        
        @param parent: The main window
        @param msg: The dialog message
        @param option_list: The combo options
        @param default: The default combo value
        '''
        
        super(InputComboBoxDialog, self).__init__(title="Requesting user information", transient_for=parent)
        self.add_button(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL)
        self.add_button(Gtk.STOCK_OK, Gtk.ResponseType.OK)

        self.set_size_request(350, 150)
        self.set_border_width(10)

        topbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        topbox.pack_start(Gtk.Label(label=msg, halign=Gtk.Align.START), True, True, 0)

        self.combo = self._create_text_combo(option_list)
        
        if default is not None and default in option_list:
            self.combo.set_active(option_list.index(default))
        else:
            self.combo.set_active(0)
            
        topbox.pack_start(self.combo, True, True, 0)

        self.get_content_area().pack_start(topbox, False, False, 0)
        self.show_all()

    def _do_validate(self):
        """
        Validate the field
        """
        if not self.combo.get_active_text():
            self._show_message('The required field is missing', 'You need to select one item to continue')
            return None

        return Gtk.ResponseType.OK

    def do_show_and_get_value(self):
        """
        Display the dialog and get the typed value
        """
        while self.run() == Gtk.ResponseType.OK:
            if self._do_validate() is not None:
                resp = self.combo.get_active_text()
                self.destroy()
                return resp

        self.destroy()
        return None
