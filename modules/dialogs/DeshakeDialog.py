# -*- coding: utf-8 -*-

'''
###############################################################################################'
 DeshakeDialog.py

 This is a free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This file is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with the software; see the file COPYING. If not, write to the
 Free Software Foundation, Inc.,
 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.

###############################################################################################'
 @class Dialog used to request the deshake settings from the user 
 @author: Luiz Oscar Machado Barbosa - <luizoscar@gmail.com>
##############################################################################################
'''
from modules.dialogs.BaseContainer import BaseDialog
from gi.repository import Gtk


class DeshakeDialog(BaseDialog):
    """
    Dialog used to request the deshake settings from the user 
    """
    SHAKINESS = 'shakiness'
    ZOOM = 'zoom'

    def __init__(self, parent):
        
        super(DeshakeDialog, self).__init__(title="Video stabilization", transient_for=parent)
        self.add_button(Gtk.STOCK_OK, Gtk.ResponseType.OK)
        self.add_button(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL)

        self.set_size_request(350, 150)
        self.set_border_width(10)

        self.debug("Requesting video stabilization settings.")

        top_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)

        # Shakiness
        box = Gtk.Box()
        box.pack_start(Gtk.Label(label="Shakiness:", halign=Gtk.Align.START), True, True, 6)

        adjustment_intensity = Gtk.Adjustment(0, 0, 10, 1, 10, 0)
        self._spinner_shakiness = Gtk.SpinButton()
        self._spinner_shakiness.set_adjustment(adjustment_intensity)
        self._spinner_shakiness.set_numeric(True)
        self._spinner_shakiness.set_value(3)

        box.pack_end(self._spinner_shakiness, True, True, 0)
        top_box.pack_start(box, True, True, 0)

        # Zoom
        box = Gtk.Box()
        box.pack_start(Gtk.Label(label="Image zoom (pixels):", halign=Gtk.Align.START), True, True, 6)

        adjustment_zoom = Gtk.Adjustment(0, 0, 10, 1, 10, 0)
        self._spinner_zoon = Gtk.SpinButton()
        self._spinner_zoon.set_adjustment(adjustment_zoom)
        self._spinner_zoon.set_numeric(True)
        self._spinner_zoon.set_value(6)

        box.pack_end(self._spinner_zoon, True, True, 0)
        top_box.pack_end(box, True, True, 0)

        self.get_content_area().pack_start(top_box, True, True, 0)
        self.show_all()

    def _do_validate(self):
        """
        Validate the user information
        """
        if self._spinner_shakiness.get_value_as_int() < 1:
            return self._show_message('Required field missing:', 'You need to specify the movement shakiness (0-10).')

        if self._spinner_zoon.get_value_as_int() < 1:
            return self._show_message('Required field missing:', 'You need to specify the image crop zoom in pixels.')

        return Gtk.ResponseType.OK

    def show_and_get_info(self):
        """
        Show the dialog
        """
        while self.run() == Gtk.ResponseType.OK:
            if self._do_validate() is not None:
                resp = {"intensity":self._spinner_shakiness.get_value_as_int(), "zoom":self._spinner_zoon.get_value_as_int()}
                self.destroy()
                return resp

        self.destroy()
        return None
