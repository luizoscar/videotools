# -*- coding: utf-8 -*-
'''
###############################################################################################'
 BaseContainer.py

 This is a free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 NTLM APS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with the software; see the file COPYING. If not, write to the
 Free Software Foundation, Inc.,
 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.

###############################################################################################'
 @class Module used as the base for all windows 
 @author: Luiz Oscar Machado Barbosa - <luizoscar@gmail.com>
##############################################################################################
'''
import logging

from gi.repository import Gtk

from modules.settings.AppSettings import AppSettings
from modules.utils.Utils import Utils


class BaseContainer(object):

    _app_settings = None

    def do_close_screen(self):
        self.destroy()
        
    def debug(self, msg=''):
        '''
        Log a debug message
        '''
        logging.info(msg)
    
    def get_app_settings(self, xml_tag):
        '''
        Retrieve a system settings
        
        @param xml_tag: The required app setting name
        
        '''
        if self._app_settings is None:
            self._app_settings = AppSettings()
        return self._app_settings.get_app_settings(xml_tag)
    
    def _show_message(self, title, msg):
        """
        Show a message to the user.
        Note: This method always return None so it can stop the processing

        @param title: The dialog title
        @param msg: The dialog message
        """
    
        self.debug("Message to the user: " + title + ": " + msg)
        dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.INFO, Gtk.ButtonsType.CLOSE, title)
        dialog.format_secondary_text(msg)
        dialog.run()
        dialog.destroy()
        return None
    
    def _ask_question(self, title, msg):
        '''
        Ask a question to the user

        @param title: The dialog title
        @param msg: The dialog message
        '''
        
        self.debug("Question to the user: " + title + ": " + msg)
        dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.QUESTION, Gtk.ButtonsType.YES_NO, title)
        dialog.format_secondary_text(msg)
        response = dialog.run()
        dialog.destroy()
        return response == Gtk.ResponseType.YES
            
    def _create_text_combo(self, options):
        """
        Create a text combo box filled with a list of options.
        
        @param options: The list of text items
        """
    
        # Create the combo
        combo_box = Gtk.ComboBoxText()

        # Add the items
        for option in options:
            combo_box.append_text(option)
    
        return combo_box
    
    def _do_hide_and_clear_combo(self, combo, label):
        """
        Clear a combo an hide his relative label.
    
        @param combo: The combo to clear
        @param label: The label to hide  
        """
        combo.remove_all()
        combo.hide()
        label.hide()
    
    def _create_icon_and_label_item(self, label, icon, action):
        """
        Create a MenuItem with an icon

        @param label: The button title
        @param icon: The GTK icon name
        @param action: The button click action method
        """
        self.debug(" - Creating MenuItem: " + label)
        button = Gtk.MenuItem.new()
        b_grid = Gtk.Grid()
        b_grid.set_column_spacing(10)
        b_grid.attach(Gtk.Image.new_from_icon_name(icon, Gtk.IconSize.BUTTON), 0, 0, 1, 1)
        b_grid.attach(Gtk.Label(label=label, halign=Gtk.Align.CENTER), 1, 0, 1, 1)
        b_grid.show_all()
        button.add(b_grid)
        button.connect("activate", action)
        return button
    
    def _create_icon_and_label_button(self, label, icon, action):
        """
        Create a button with an icon

        @param label: The button title
        @param icon: The GTK icon name
        @param action: The button click action method
        """
    
        self.debug(" - Creating button: " + label)
        button = Gtk.Button.new()
        b_grid = Gtk.Grid()
        b_grid.set_column_spacing(10)
        b_grid.set_row_homogeneous(True)
        b_grid.attach(Gtk.Image.new_from_icon_name(icon, Gtk.IconSize.BUTTON), 0, 0, 1, 1)
        b_grid.attach(Gtk.Label(label=label, halign=Gtk.Align.CENTER), 1, 0, 1, 1)
        b_grid.show_all()
        button.add(b_grid)
        button.connect("clicked", action)
        return button
    
    def _compare_tree_rows(self, model, row1, row2, user_data):  # @UnusedVariable
        """
        Sort method used to compare TreeView rows.
        
        @param model: The tree model
        @param row1: The first row  
        @param row2: The second row  
        @param user_data: Tree data  
        """
        sort_column, _ = model.get_sort_column_id()
        value1 = model.get_value(row1, sort_column)
        value2 = model.get_value(row2, sort_column)
    
        if value1 < value2:
            return -1
        elif value1 == value2:
            return 0
        else:
            return 1


class BaseWindow(Gtk.Window, BaseContainer):
    '''
    Base class for GTK Window
    '''

    def __init__(self, **kwds):
        super(BaseWindow, self).__init__(**kwds)
        self.utils = Utils()

    
class BaseDialog(Gtk.Dialog, BaseContainer):
    '''
    Base class for GTK Dialogs
    '''

    def __init__(self, **kwds):
        super(BaseDialog, self).__init__(**kwds)
        self.utils = Utils()

