# -*- coding: utf-8 -*-

'''
###############################################################################################'
 VideoProgressDialog.py

 This is a free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This file is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with the software; see the file COPYING. If not, write to the
 Free Software Foundation, Inc.,
 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.

###############################################################################################'
 @class Dialog used to show the video processing progress. 
 @author: Luiz Oscar Machado Barbosa - <luizoscar@gmail.com>
##############################################################################################
'''
import os
import datetime
import time
from threading import Thread
import subprocess

from gi.repository import Gtk, GLib

from modules.dialogs.BaseContainer import BaseDialog
from modules.settings.AppSettings import AppSettings
from modules.utils.Utils import Utils


class VideoProgressDialog(BaseDialog):    
    # FFmpeg text constants
    _DURATION = "Duration:"
    _FRAME = "frame="
    _TIME = "time="
    _NA = "N/A"

    must_stop = False
    failed = False
    ffmpeg_params = []
    file_suffix = None
    destination_file = None
    total_seconds = 0
    elapsed_seconds = 0

    def __init__(self, parent, file_list, destination_dir, title, ffmpeg_params, total_seconds):
        
        """
        Dialog used to show the video processing progress
        @param parent: The parent container
        @param file_list: The list of files that will be processed
        @param destination_dir: The directory where the new files are going to be created
        @param title: The operation title
        @param ffmpeg_params: Any params that should be passed to the ffmpeg
        @param total_seconds: The number of seconds on the selected videos     
        """    
        super(VideoProgressDialog, self).__init__(title=title, transient_for=parent)
        self.add_button(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL)
        
        self.set_size_request(250, 150)
        self.set_border_width(10)

        self.app_settings = AppSettings()
        self.utils = Utils()

        self.file_list = file_list
        self.destination_dir = destination_dir
        self.ffmpeg_params = ffmpeg_params
        self.total_seconds = total_seconds

        # Main container
        grid = Gtk.Grid()
        grid.set_column_homogeneous(True)
        grid.set_row_homogeneous(True)
        grid.set_column_spacing(4)
        grid.set_row_spacing(6)

        # Make sure to enter the loop if the destination_dir file is specified
        if self.destination_file is not None:
            self.file_list = [self.destination_file]

        total_bytes = 0
        for file_name in self.file_list:
            if os.path.isfile(file_name):
                total_bytes += os.stat(file_name).st_size

        # Activity label
        grid.attach(Gtk.Label(label="Processing " + str(len(self.file_list)) + 
                              " videos - " + self.utils.seconds_to_time(total_seconds) + " (" + self.utils.to_human_size(total_bytes) + ")", halign=Gtk.Align.START), 0, 0, 6, 1)

        # Total progress
        self.progress_bar_total = Gtk.ProgressBar(show_text=True)
        grid.attach(self.progress_bar_total, 0, 1, 6, 1)

        # Total progress label
        self.label_total_progress = Gtk.Label(halign=Gtk.Align.START)
        grid.attach(self.label_total_progress, 0, 2, 6, 1)

        # Current file progress
        self.progress_bar_current = Gtk.ProgressBar(show_text=True)
        grid.attach(self.progress_bar_current, 0, 3, 6, 1)

        # Current file title
        self.label_current_progress = Gtk.Label(halign=Gtk.Align.START)
        grid.attach(self.label_current_progress, 0, 4, 6, 1)

        self.get_content_area().pack_start(grid, True, True, 0)
    
    def process_files(self):
        """
        Start processing the files
        """
        self.show_all()

        # file converter thread
        thread = Thread(target=self._process_video_files)
        thread.daemon = True
        thread.start()
        self.run()
    
    def set_file_suffix(self,file_suffix):
        """
        Set the output file suffix

        @param file_suffix: The converted file suffix
        """
        self.file_suffix = file_suffix
    
    def set_destination_file(self, destination_file):
        """
        Set the name of the output file
    
        @param destination_file: The destination file (if single file operation)
        """
        self.destination_file = destination_file

    def _update_file_progess(self, total_progressbar_title, total_label_title, current_progressbar_title, current_file_progress_percent, total_file_progress_percent):
        """
        Update the file operation progress
        """
        # Update the counters
        self.progress_bar_total.set_text(total_progressbar_title)
        self.label_total_progress.set_text(total_label_title)
        self.label_current_progress.set_text(current_progressbar_title)

        # Update the progress bars
        self.progress_bar_current.set_fraction(current_file_progress_percent)  # Must be between 0.0 and 1.0
        self.progress_bar_total.set_fraction(total_file_progress_percent)  # Must be between 0.0 and 1.0

        return False
    
    def _is_source_missing(self, file_name):
        """
        Check if the source file is missing
        
        @param file_name: The source file name
        """
        if not os.path.isfile(file_name) and self.destination_file is None:
            self.debug("Ignoring missing file: " + file_name)
            self.failed = True
            return True
        return False
    
    def _create_dir(self, dir_name):
        """
        Create the destination dir, if it does not exists
        
        @param dir_name: The destination dir
        """
        
        # Create the destination dir, if it does not exists
        if not os.path.exists(dir_name):
            self.debug("Creating destination directory " + dir_name)
            os.makedirs(dir_name)
    
    def _remove_existing_file(self, new_file_name):
        """
        Remove the destination file, if it exists
        
        @param new_file_name: The output file name
        """
        
        # Check if the destination file already exists
        if os.path.isfile(new_file_name):
            self.debug("Removing existing file: " + new_file_name)
            os.remove(new_file_name)
    
    def _get_program_params(self, source_file,new_file_name,extension ):
        """
        Get the parameters for ffmpeg
        
        @param source_file: The path to the source file
        @param new_file_name: The output file name
        @param extension: The file extension
        """
        
        # Locate ffmpeg
        app = self.app_settings.get_app_settings(AppSettings.CFG_PATH_TO_FFMPEG)
        app = app if app is not None else "ffmpeg"

        # Build the ffmpeg params
        args = [app, "-hide_banner"]
        args.extend(self.ffmpeg_params)

        # Replace the params variables
        for idx, val in enumerate(args):  # @UnusedVariable
            args[idx] = args[idx].replace("${SOURCE}", source_file)
            args[idx] = args[idx].replace("${DESTINATION}", new_file_name)
            args[idx] = args[idx].replace("${EXTENSION}", extension)
        
        return args
    
    def _process_video_files(self):
        """
        Process each file on the file list        
        """
        for source_file in self.file_list:
            # Check if the source file exists and do not need to stop
            if not self.must_stop and not self._is_source_missing(source_file):
                self._process_file(source_file)

        self.close()
    
    def _process_file(self, source_file):
        """
        Process a file
        
        @param source_file: The file to be processed
        """
        try:
            # Get the file extension
            name = os.path.basename(source_file)
            extension = name[name.rfind(".") + 1:]

            # Use the current file extension, if the destination extension was not specified
            file_suffix = extension if not self.file_suffix else self.file_suffix

            # Build the output file name
            new_file_name = self.destination_dir + os.sep + name[:name.rfind(".")] + file_suffix
            new_file_name = new_file_name.replace("${EXTENSION}", extension)
            if self.destination_file is not None:
                new_file_name = self.destination_file

            args = self._get_program_params(source_file, new_file_name, extension)
            
            self._create_dir(os.path.dirname(new_file_name))
            self._remove_existing_file(new_file_name)

            max_secs = 0
            cur_secs = 0

            # Check if the user interrupted
            if self.must_stop:
                return None

            # Process the file
            self.debug("Running application: " + ' '.join(args))

            self.ffmpeg_process = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, bufsize=1, universal_newlines=True)

            # Run the program and iterate trough the output lines
            for line in iter(self.ffmpeg_process.stdout.readline, ''):
                if self._DURATION in line:
                    # This line has the total file time
                    max_secs = self._get_total_seconds(line)
                elif line.startswith(self._FRAME) and self._TIME in line:
                    # This line has the current progress time
                    cur_secs = self._get_current_seconds(line)

                # Update the progress
                if cur_secs > 0 and max_secs > 0:
                    total_progress = (self.elapsed_seconds + cur_secs) / self.total_seconds  # Total progress
                    current_progress = cur_secs / max_secs
                    total_bar_title = "[" + self.utils.seconds_to_time(self.elapsed_seconds + cur_secs) + " / " + self.utils.seconds_to_time(self.total_seconds) + "]"

                    total_label_title = "From: " + os.path.basename(source_file) + " - " + self.utils.seconds_to_time(max_secs)
                    if os.path.isfile(new_file_name):
                        total_label_title = total_label_title + " (" + self.utils.to_human_size(os.stat(source_file).st_size) + ")"

                    current_label_title = "To: " + os.path.basename(new_file_name) + " - " + self.utils.seconds_to_time(cur_secs)
                    if os.path.isfile(new_file_name):
                        current_label_title = current_label_title + " (" + self.utils.to_human_size(os.stat(new_file_name).st_size) + ")"

                    GLib.idle_add(self._update_file_progess, total_bar_title, total_label_title, current_label_title, current_progress, total_progress)

            # After processing the file, increment the total time 
            self.elapsed_seconds = self.elapsed_seconds + cur_secs

            # Closes the ffmpeg process
            self.ffmpeg_process.stdout.close()
            exit_code = self.ffmpeg_process.wait()
            
            # Check the exit code
            self.failed = self.failed or exit_code != 0
            if self.failed:
                self.debug("Error message: " + line)
            else:
                self._show_file_status(source_file, new_file_name)

        except Exception as e:
            self.debug("Failed to process the file " + source_file + " : " + str(e))
            self.failed = True        
    
    def _get_current_seconds(self, line):
        """
        Extract the current operation time stamp
        
        @param line: The line to be parsed
        """
        
        try:
            # Get the conversion time
            tmp = line[line.find(self._TIME):]
            tmp = tmp[tmp.find("=") + 1: tmp.find(".")]
            x = time.strptime(tmp, '%H:%M:%S')
            return datetime.timedelta(hours=x.tm_hour, minutes=x.tm_min, seconds=x.tm_sec).total_seconds()
        except ValueError:
            self.debug("Failed to convert the time stamp: " + tmp)
            return 0
        
    
    def _get_total_seconds(self, line):
        """
        Extract the total operation time stamp
        
        @param line: The line to be parsed
        """
        try:
            if self._NA in line:
                return self.total_seconds
            else:
                tmp = line[line.find(self._DURATION):]
                tmp = tmp[tmp.find(" ") + 1:]
                tmp = tmp[0: tmp.find(".")]
                x = time.strptime(tmp, '%H:%M:%S')
                return datetime.timedelta(hours=x.tm_hour, minutes=x.tm_min, seconds=x.tm_sec).total_seconds()

        except ValueError:
            self.debug("Failed to convert the timestamp from the line: " + line)
    
    def _show_file_status(self, source_file, new_file_name):
        """
        Show the file conversion status
        
        @param source_file: The source file name
        @param new_file_name: The output file name
        """
        if os.path.isfile(source_file):
            self.debug("Source file: " + source_file + " (" + self.utils.to_human_size(os.stat(source_file).st_size) + ")")

        if os.path.isfile(new_file_name):
            self.debug("Processed file: " + new_file_name + " (" + self.utils.to_human_size(os.stat(new_file_name).st_size) + ")")
    
    def interrrupt_process(self):
        '''
        Make sure ffmpeg process is finished!
        '''    
        if self.ffmpeg_process is not None:
            try:
                self.ffmpeg_process.kill()
                self.debug("The ffmpeg process was interrupted by the user.")
            except OSError:
                self.debug("The ffmpeg process was successfully terminated.")
                
