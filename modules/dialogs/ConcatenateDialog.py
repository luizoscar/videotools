# -*- coding: utf-8 -*-

'''
###############################################################################################'
 ConcatenateDialog.py

 This is a free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This file is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with the software; see the file COPYING. If not, write to the
 Free Software Foundation, Inc.,
 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.

###############################################################################################'
 @class Dialog used to concatenate multiple files 
 @author: Luiz Oscar Machado Barbosa - <luizoscar@gmail.com>
##############################################################################################
'''
from modules.dialogs.BaseContainer import BaseDialog
from gi.repository import Gtk
import os


class ConcatenateDialog(BaseDialog):
    """
    Dialog to request the user for the sequence of the files that will be concatenated
    """
    # Application dir
    _APP_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
    _TEMP_FILE = _APP_DIR + os.sep + "temp_file_list.txt"
    _codec_settings = None
    
    CFG_TARGET_FILE = 'file'
    CFG_TARGET_CODEC = 'codec'
    CFG_TEMP_FILE = 'temp'
    
    def __init__(self, parent, file_list, destination_dir, codec_settings):
        """
        Default constructor
        
        @param parent: The parent window
        @param file_list: The selected files
        @param destination_dir: The destination dir
        @param codec_settings: The output file codec settings
        """
        super(ConcatenateDialog, self).__init__(title="Video concatenation file list", transient_for=parent)
        self.add_button(Gtk.STOCK_OK, Gtk.ResponseType.OK)
        self.add_button(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL)

        self._codec_settings = codec_settings
        self.set_size_request(600, 480)
        self.set_border_width(10)

        # Remove the temp file
        if os.path.isfile(self._TEMP_FILE):
            os.remove(self._TEMP_FILE)

        # File list
        self.textview = Gtk.TextView()

        scrolledwindow = Gtk.ScrolledWindow()
        scrolledwindow.set_hexpand(True)
        scrolledwindow.set_vexpand(True)
        scrolledwindow.add(self.textview)

        self.grid = Gtk.Grid()
        self.grid.attach(scrolledwindow, 0, 0, 6, 6)

        # Load the file list
        tmp = ""
        for file_name in file_list:
            tmp = tmp + "file " + file_name + "\n"

        self.textview.get_buffer().set_text(tmp)

        # Output codec combo
        flowbox = Gtk.FlowBox()
        flowbox.set_selection_mode(Gtk.SelectionMode.NONE)       
        flowbox.add(Gtk.Label(label="Output file codec:", halign=Gtk.Align.START))

        self.combo_codec = self._create_text_combo(self._codec_settings.get_video_codec_names())
        self.combo_codec.set_active(0)
        flowbox.add(self.combo_codec)

        self.grid.attach(flowbox, 0, 7, 3, 1)

        # Output file name
        box = Gtk.Box()
        box.pack_start(Gtk.Label(label="Output file name:", halign=Gtk.Align.START), False, False, 4)
        self.edit_target_file = Gtk.Entry()
        self.edit_target_file.set_text(destination_dir + os.sep + "concatenated.mp4")
        box.pack_end(self.edit_target_file, True, True, 4)

        self.grid.attach(box, 0, 8, 6, 1)

        self.get_content_area().pack_start(self.grid, True, True, 0)

        self.show_all()

    def show_and_get_info(self):
        """
        Show the dialog
        """
        while self.run() == Gtk.ResponseType.OK:
            if self.edit_target_file.get_text() is not None:

                buf = self.textview.get_buffer()
                text = buf.get_text(buf.get_start_iter(),
                        buf.get_end_iter(),
                        True)
                open(self._TEMP_FILE, 'w').write(text)
                
                codec = self.codec_settings.get_codec_info(self.combo_codec.get_active_text())
                resp = {self.CFG_TARGET_FILE:self.edit_target_file.get_text(),
                        self.CFG_TARGET_CODEC:codec,
                        self.CFG_TEMP_FILE:self._TEMP_FILE
                        }
                self.destroy()

                return resp

        self.destroy()
        return None
