# -*- coding: utf-8 -*-
'''
###############################################################################################'
 TextViewerDialog.py

 This is a free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This file is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with the software; see the file COPYING. If not, write to the
 Free Software Foundation, Inc.,
 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.

###############################################################################################'
 @class Dialog used to display a multi-line text
 @author: Luiz Oscar Machado Barbosa - <luizoscar@gmail.com>
##############################################################################################
'''

from gi.repository import Gtk

from modules.dialogs.BaseContainer import BaseDialog

class TextViewerDialog(BaseDialog):
    """
    Dialog used to display a text editor
    """

    def __init__(self, parent):
        '''
        Default constructor
        
        @param parent: The main window
        '''

        super(TextViewerDialog, self).__init__(title="Text Viewer",transient_for=parent)
        self.add_button(Gtk.STOCK_OK, Gtk.ResponseType.OK)
        
        self.debug("TextViewerDialog - Dialog displayed")

        self.set_size_request(1024, 600)
        self.set_border_width(10)

        scrolledwindow = Gtk.ScrolledWindow()
        scrolledwindow.set_hexpand(True)
        scrolledwindow.set_vexpand(True)

        self.grid = Gtk.Grid()
        self.grid.attach(scrolledwindow, 0, 1, 3, 1)

        self.textview = Gtk.TextView()
        self.textview.set_property('editable', False)
        scrolledwindow.add(self.textview)

        self.get_content_area().pack_start(self.grid, True, True, 0)
        self.show_all()
    
    def set_text(self, text):
        '''
        Set the editor text.
        '''
        self.textview.get_buffer().set_text(text)
        
    def do_show_window(self):
        self.run()
        self.destroy()
        self.debug("TextViewerDialog - Dialog Closed")
        return None
