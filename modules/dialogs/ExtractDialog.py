# -*- coding: utf-8 -*-

'''
###############################################################################################'
 ExtractDialog.py

 This is a free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This file is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with the software; see the file COPYING. If not, write to the
 Free Software Foundation, Inc.,
 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.

###############################################################################################'
 @class Dialog used to extract a time section of a video file 
 @author: Luiz Oscar Machado Barbosa - <luizoscar@gmail.com>
##############################################################################################
'''
from modules.dialogs.BaseContainer import BaseDialog
from gi.repository import Gtk
import re


class ExtractDialog(BaseDialog):
    """
    Dialog used to extract a time section of a video file
    """
    _video_duration = None
    
    CFG_START = 'start'
    CFG_END = 'end'
    
    def __init__(self, parent, start_position, video_duration):
        '''
        Default constructor
        
        @param parent: The main window
        @param start_position: The new video start time
        @param video_duration: The video duration
        '''
        
        super(ExtractDialog, self).__init__(title="Section from the video to be extracted", transient_for=parent)
        self.add_button(Gtk.STOCK_OK, Gtk.ResponseType.OK)
        self.add_button(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL)
        
        self.set_size_request(350, 150)
        self.set_border_width(10)

        self._video_duration = video_duration

        self.debug("Requesting the new video section")

        top_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)

        # Start
        box = Gtk.Box()
        box.pack_start(Gtk.Label(label="Video start time:", halign=Gtk.Align.START), True, True, 6)
        self._start_edit = Gtk.Entry()
        self._start_edit.set_text(start_position)
        box.pack_end(self._start_edit, True, True, 0)
        top_box.pack_start(box, True, True, 0)

        # End
        box = Gtk.Box()
        box.pack_start(Gtk.Label(label="Video end time:", halign=Gtk.Align.START), True, True, 6)
        self._end_edit = Gtk.Entry()
        self._end_edit.set_text(video_duration)
        box.pack_end(self._end_edit, True, True, 0)
        top_box.pack_start(box, True, True, 0)

        self.get_content_area().pack_start(top_box, True, True, 0)
        self.show_all()

    def _do_validate(self):
        """
        Validate the user information
        """        
        start = self._start_edit.get_text().strip()
        end = self._end_edit.get_text().strip()

        pattern = "[0-9]{2}:[0-9]{2}:[0-9]{2}"

        if re.search(pattern, start) is None:
            return self._show_message('Invalid date format:', 'You need to specify the start time in the format hh:mm:ss.')

        if re.search(pattern, end) is None:
            return self._show_message('Invalid date format:', 'You need to specify the end time in the format hh:mm:ss.')

        if self.utils.time_to_seconds(start) >= self.utils.time_to_seconds(end):
            return self._show_message('Invalid values:', 'The end time must happen after the start time.')

        if self.utils.time_to_seconds(start) >= self.utils.time_to_seconds(self._video_duration) or self.utils.time_to_seconds(end) > self.utils.time_to_seconds(self._video_duration):
            return self._show_message('Invalid values:', "The values must be smaller than " + self._video_duration)

        return Gtk.ResponseType.OK

    def show_and_get_info(self):
        """
        Show the dialog
        """
        while self.run() == Gtk.ResponseType.OK:
            if self._do_validate() is not None:
                resp = {self.CFG_START:self._start_edit.get_text().strip(),
                        self.CFG_END:self._end_edit.get_text().strip()}
                self.destroy()
                return resp

        self.destroy()
        return None
