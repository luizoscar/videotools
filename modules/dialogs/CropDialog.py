# -*- coding: utf-8 -*-

'''
###############################################################################################'
 CropDialog.py

 This is a free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This file is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with the software; see the file COPYING. If not, write to the
 Free Software Foundation, Inc.,
 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.

###############################################################################################'
 @class Dialog used to request the crop settings from the user 
 @author: Luiz Oscar Machado Barbosa - <luizoscar@gmail.com>
##############################################################################################
'''
from modules.dialogs.BaseContainer import BaseDialog
from gi.repository import Gtk


class CropDialog(BaseDialog):
    """
    Dialog used to request the crop settings
    """
    X = 'x'
    Y = 'y'
    WIDTH = 'width'
    HEIGHT = 'height'
    
    def __init__(self, parent, width, height):
        '''
        Default constructor
        
        @param parent: The main window
        @param width: The video max width
        @param height: The video max height 
        '''
        
        super(CropDialog, self).__init__(title="Crop a video region", transient_for=parent)
        self.add_button(Gtk.STOCK_OK, Gtk.ResponseType.OK)
        self.add_button(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL)

        self.set_size_request(350, 150)
        self.set_border_width(10)

        self._width = width
        self._height = height

        self.debug("Requesting crop settings.")

        top_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)

        # Vertical starting position
        box = Gtk.Box()
        box.pack_start(Gtk.Label(label="Vertical start (X):", halign=Gtk.Align.START), True, True, 6)
        adjustment_x = Gtk.Adjustment(0, 0, height, 10, 100, 0)
        self._spinner_shakiness = Gtk.SpinButton()
        self._spinner_shakiness.set_adjustment(adjustment_x)
        self._spinner_shakiness.set_numeric(True)
        box.pack_end(self._spinner_shakiness, True, True, 0)
        top_box.pack_start(box, True, True, 0)

        # Horizontal starting position
        box = Gtk.Box()
        box.pack_start(Gtk.Label(label="Horizontal start (Y):", halign=Gtk.Align.START), True, True, 6)
        adjustment_y = Gtk.Adjustment(0, 0, width, 10, 100, 0)
        self._spin_y = Gtk.SpinButton()
        self._spin_y.set_adjustment(adjustment_y)
        self._spin_y.set_numeric(True)
        box.pack_end(self._spin_y, True, True, 0)
        top_box.pack_start(box, True, True, 0)

        # Width
        box = Gtk.Box()
        box.pack_start(Gtk.Label(label="Width:", halign=Gtk.Align.START), True, True, 6)
        adjustment_w = Gtk.Adjustment(width, 1, width, 10, 100, 0)
        self._spin_w = Gtk.SpinButton()
        self._spin_w.set_adjustment(adjustment_w)
        self._spin_w.set_numeric(True)
        box.pack_end(self._spin_w, True, True, 0)
        top_box.pack_start(box, True, True, 0)

        # Height
        box = Gtk.Box()
        box.pack_start(Gtk.Label(label="Height:", halign=Gtk.Align.START), True, True, 6)
        adjustment_h = Gtk.Adjustment(height, 1, height, 10, 100, 0)
        self._spin_h = Gtk.SpinButton()
        self._spin_h.set_adjustment(adjustment_h)
        self._spin_h.set_numeric(True)
        box.pack_end(self._spin_h, True, True, 0)
        top_box.pack_start(box, True, True, 0)

        self.get_content_area().pack_start(top_box, True, True, 0)
        self.show_all()

    def show_and_get_info(self):
        """
        Show the dialog
        """        
        while self.run() == Gtk.ResponseType.OK:
            resp = {self.X:self._spinner_shakiness.get_value() ,
                    self.Y:self._spin_y.get_value(),
                    self.WIDTH:self._spin_w.get_value(),
                    self.HEIGHT:self._spin_h.get_value()}
            self.destroy()
            return resp

        self.destroy()
        return None
