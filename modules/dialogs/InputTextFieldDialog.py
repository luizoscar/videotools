# -*- coding: utf-8 -*-
'''
###############################################################################################'
 InputTextFieldDialog.py

 This is a free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This file is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with the software; see the file COPYING. If not, write to the
 Free Software Foundation, Inc.,
 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.

###############################################################################################'
 @class Dialog used request information from the user using a Text Field 
 @author: Luiz Oscar Machado Barbosa - <luizoscar@gmail.com>
##############################################################################################
'''
from gi.repository import Gtk

from modules.dialogs.BaseContainer import BaseDialog


class InputTextFieldDialog(BaseDialog):
    """
    Input dialog using Text field
    """
    def __init__(self, parent, msg, value=None):
        '''
        Default constructor
        
        @param parent: The main window
        @param msg: The dialog message
        @param value: The TextField value
        '''
        super(InputTextFieldDialog, self).__init__(title="Requesting user information", transient_for=parent)
        self.add_button(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL)
        self.add_button(Gtk.STOCK_OK, Gtk.ResponseType.OK)
        
        self.set_size_request(350, 150)
        self.set_border_width(10)

        topbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        topbox.pack_start(Gtk.Label(label=msg, halign=Gtk.Align.START), True, True, 0)

        # Text field
        self.text_field = Gtk.Entry()
        if value is not None:
            self.text_field.set_text(value)
        topbox.pack_start(self.text_field, True, True, 0)

        self.get_content_area().pack_start(topbox, False, False, 0)
        self.show_all()

    def _do_validate(self):
        """
        Validate the field
        """
        if self.text_field is not None and not self.text_field.get_text():
            self._show_message('The required text is missing', 'You need to specify the required text value')
            return None

        return Gtk.ResponseType.OK

    def do_show_and_get_value(self):
        """
        Display the dialog and get the value
        """
        while self.run() == Gtk.ResponseType.OK:
            if self._do_validate() is not None:
                resp = self.text_field.get_text().strip()
                self.destroy()
                return resp

        self.destroy()
        return None
