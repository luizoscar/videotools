# -*- coding: utf-8 -*-

'''
###############################################################################################'
 MainWindow.py

 This is a free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This file is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with the software; see the file COPYING. If not, write to the
 Free Software Foundation, Inc.,
 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.

###############################################################################################'
 @class Dialog used to show the video processing progress. 
 @author: Luiz Oscar Machado Barbosa - <luizoscar@gmail.com>
##############################################################################################
'''
import time
import re
import os
import sys
import subprocess
from gi.repository import Gdk, Gtk
from os.path import dirname, abspath

from modules.dialogs.TextViewerDialog import TextViewerDialog
from modules.dialogs.CropDialog import CropDialog
from modules.dialogs.InputComboBoxDialog import InputComboBoxDialog
from modules.dialogs.ConcatenateDialog import ConcatenateDialog
from modules.dialogs.ExtractDialog import ExtractDialog
from modules.dialogs.DeshakeDialog import DeshakeDialog
from modules.dialogs.VideoProgressDialog import VideoProgressDialog
from modules.settings.CodecSettings import CodecSettings, CodecInfo
from modules.utils.Utils import Utils
from modules.dialogs.InputTextFieldDialog import InputTextFieldDialog
from modules.settings.AppSettings import AppSettings
from modules.dialogs.BaseContainer import BaseWindow
from modules.dialogs.FileListReaderDialog import FileListReaderDialog


class MainWindow(BaseWindow):
    _APP_VERSION = "v1.1" 
    _GRID_COL_NAMES = ["Selected", "File", "Size", "Details"]
    _button_list = []
    _popup_menu = Gtk.Menu()
    _sub_menu = Gtk.Menu()
    _logger = None
    _ffmpeg_features = None
    _codec_settings = None
    _SOURCE = "${SOURCE}"
    _DESTINATION = "${DESTINATION}"
    _EXTENSION = "${EXTENSION}"
    _REGEX_TIMESTAMP = "([0-9]{2,}x[0-9]{2,})"
    _REGEX_FEATURES_FFMPEG = "--enable-[^\s]+|disable-[^\s]+"
    _file_map = {}

    def __init__(self):
        Gtk.Window.__init__(self, title="Video Tools - " + self._APP_VERSION)

        self.set_icon_name("application-x-executable")
        Gtk.Settings().set_property('gtk_button_images', True)

        # Clipboard
        self.clipboard = Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD)
        self._codec_settings = CodecSettings(self._get_ffmpeg_features())

        self.set_resizable(True)
        self.set_border_width(10)

        self.connect('delete-event', self._on_close)

        # Main container
        grid = Gtk.Grid()
        grid.set_column_homogeneous(True)
        grid.set_row_homogeneous(True)
        grid.set_column_spacing(4)
        grid.set_row_spacing(6)

        self.app_settings = AppSettings()
        self.utils = Utils()
        
        # Source folder text field 
        self.edit_source_dir = Gtk.Entry()
        self.edit_source_dir.set_activates_default(True)
        self.edit_source_dir.set_text(self.app_settings.get_app_settings(AppSettings.CFG_SOURCE_DIR))

        # Select folder button
        button = Gtk.Button.new_from_icon_name("folder-open", Gtk.IconSize.BUTTON)
        button.connect("clicked", self._do_select_source_dir)

        box = Gtk.Box()
        box.pack_start(Gtk.Label(label="Source directory:", halign=Gtk.Align.START), False, False, 0)
        box.pack_start(self.edit_source_dir, True, True, 4)
        box.pack_end(button, False, False, 0)

        grid.attach(box, 0, 0, 6, 1)

        # Refresh button
        self.button_refersh = self._create_icon_and_label_button("Refresh", "view-refresh", self._do_load_file_list)
        grid.attach(self.button_refersh, 6, 1, 1, 1)

        # Convert button
        self.button_convert = self._create_icon_and_label_button("Convert", "video-x-generic", self._do_video_convert)
        self._button_list.append(self.button_convert)
        grid.attach(self.button_convert, 6, 3, 1, 1)

        # Resize button
        self.button_resize = self._create_icon_and_label_button("Resize", "view-restore", self._do_video_resize)
        self._button_list.append(self.button_resize)
        grid.attach(self.button_resize, 6, 4, 1, 1)

        # Rotate button
        self.button_rotate = self._create_icon_and_label_button("Rotate", "object-rotate-left", self._do_video_rotate)
        self._button_list.append(self.button_rotate)
        grid.attach(self.button_rotate, 6, 5, 1, 1)

        # Extract interval button
        self.button_extract_interval = self._create_icon_and_label_button("Interval", "appointment-soon", self._do_video_extract_interval)
        self._button_list.append(self.button_extract_interval)
        grid.attach(self.button_extract_interval, 6, 6, 1, 1)

        # Extract region button
        self.button_extract_section = self._create_icon_and_label_button("Crop", "object-flip-horizontal", self._do_video_extract_region)
        self._button_list.append(self.button_extract_section)
        grid.attach(self.button_extract_section, 6, 7, 1, 1)

        # Concatenate button
        self.button_concatenate = self._create_icon_and_label_button("Concatenate", "list-add", self._do_video_concatenate)
        self._button_list.append(self.button_concatenate)
        grid.attach(self.button_concatenate, 6, 8, 1, 1)

        # Stabilize button
        if "--enable-libvidstab" in self._get_ffmpeg_features():
            self.button_deshake = self._create_icon_and_label_button("Stabilize", "media-playlist-shuffle", self._do_video_deshake)
            self._button_list.append(self.button_deshake)
            grid.attach(self.button_deshake, 6, 9, 1, 1)

        # Logs button
        grid.attach(self._create_icon_and_label_button("Logs", "system-search", self._do_click_logs), 6, 11, 1, 1)

        # Close button
        grid.attach(self._create_icon_and_label_button("Close", "window-close", self._on_close), 6, 12, 1, 1)

        # File list
        self.store = Gtk.ListStore(bool, str, str, str)

        self.filter = self.store.filter_new()
        cell_renderer = Gtk.CellRendererText()

        # The the grid columns
        self.treeview = Gtk.TreeView(model=self.store)
        self.treeview.connect("button_press_event", self._do_show_popup)

        # Note: Cols 0 and 1 are not simple String
        col1 = Gtk.TreeViewColumn("Selected", Gtk.CellRendererToggle(), active=0)
        col1.set_sort_column_id(0)
        self.treeview.append_column(col1)

        # Add the remaining columns
        for i, column_title in enumerate(self._GRID_COL_NAMES):
            column = Gtk.TreeViewColumn(column_title, cell_renderer, text=i)
            if i > 0:  # Col 0 was added before
                self.treeview.append_column(column)
            self.store.set_sort_func(i, self._compare_tree_rows, None)
            column.set_sort_column_id(i)

        self.treeview.connect("row-activated", self._on_tree_double_clicked)

        # Add the tree to a scroll window
        scrollable_treelist = Gtk.ScrolledWindow()
        scrollable_treelist.set_vexpand(True)
        scrollable_treelist.add(self.treeview)
        grid.attach(scrollable_treelist, 0, 1, 6, 12)

        # File selection status label
        self.label_status = Gtk.Label(label="", halign=Gtk.Align.START)
        grid.attach(self.label_status, 0, 13, 7, 1)

        self.add(grid)
        self._do_update_counters()
        
        # Popup menu
        i1 = Gtk.MenuItem("Check all videos")
        i1.connect("activate", self._do_select_all)
        self._popup_menu.append(i1)
        i2 = Gtk.MenuItem("Uncheck all videos")
        i2.connect("activate", self._do_unselect_all)
        self._popup_menu.append(i2)
        i3 = Gtk.MenuItem("Check videos from format")
        
        decoders = CodecSettings(self._get_ffmpeg_features()).get_video_decoders()
        for decoder in decoders:
            sub = Gtk.MenuItem(decoder[0])
            sub.connect("activate", self._do_select_by_decoder)
            self._sub_menu.append(sub)
            
        i3.set_submenu(self._sub_menu)
        self._popup_menu.append(i3)
        i4 = Gtk.MenuItem("Delete the selected files")
        i4.connect("activate", self._do_delete_selected_files)
        self._popup_menu.append(i4)
        self._popup_menu.show_all()
        
        # Load the screen position and size from previous execution
        self.edit_source_dir.set_text(self.app_settings.get_app_settings(AppSettings.CFG_SOURCE_DIR))
        self.video_extensions = self.app_settings.get_app_settings(AppSettings.CFG_VIDEO_EXTENSIONS).split('|');    

        x = self.app_settings.get_app_settings(AppSettings.WINDOW_X)
        x = int(x) if x else 200
        
        y = self.app_settings.get_app_settings(AppSettings.WINDOW_Y)
        y = int(y) if y else 200
        
        w = self.app_settings.get_app_settings(AppSettings.WINDOW_W)
        w = int(w) if w else 1280
        
        h = self.app_settings.get_app_settings(AppSettings.WINDOW_H)
        h = int(h) if h else 450
        
        self.set_default_size(w, h)
        self.move(x, y)

    def _do_show_popup(self, widget, event):  # @UnusedVariable
        """
        Open the popup menu over the file grid
        
        @param widget: The widget that triggered the action
        @param event: The mouse event
        """
        #Note: Button 3 is Right button
        if event.button == 3:
            self._popup_menu.popup(None, None, None, None, 0, Gtk.get_current_event_time())

    def _do_delete_selected_files(self, widget):  # @UnusedVariable
        """
        Popup menu action to delete the selected files
        
        @param widget: The widget that triggered the action
        """
        self.debug("MenuItem: Delete the selected files")
        file_list = self._get_selected_files_list()
        if len(file_list) > 0:
            dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.QUESTION, Gtk.ButtonsType.YES_NO, "Delete the selected files")
            dialog.format_secondary_text("Are you sure that you want to permanently delete the" + str(len(file_list)) + " selected files?")
            response = dialog.run()
            if response == Gtk.ResponseType.YES:
                for file_name in file_list:
                    self.debug("Removing file " + file_name)
                    os.remove(file_name)
                self._do_load_file_list(widget)
            dialog.destroy()

    def _do_select_by_decoder(self, widget):  # @UnusedVariable
        """
        Popup menu action to select the videos by decoder
        
        @param widget: The widget that triggered the action
        """
        codec_title = widget.get_label()
        codec_name = "h264"
        self.debug("MenuItem: Check videos from "+codec_title)
        
        for decoder in CodecSettings(self._get_ffmpeg_features()).get_video_decoders():
            if decoder[0] == codec_title:
                codec_name = decoder[1] 

        for row in self.store:
            if codec_name in row[3]:
                row[0] = True

        self._do_update_counters()

    def _do_select_all(self, widget):  # @UnusedVariable
        """
        Popup menu action to select all videos
        
        @param widget: The widget that triggered the action
        """
        self.debug("MenuItem: Select all videos")
        for row in self.store:
            row[0] = True

        self._do_update_counters()

    def _do_unselect_all(self, widget):  # @UnusedVariable
        """
        Popup menu action to unselect all videos
        
        @param widget: The widget that triggered the action
        """
        self.debug("MenuItem: Unselect all videos")
        for row in self.store:
            row[0] = False

        self._do_update_counters()

    def _do_select_source_dir(self, widget):  # @UnusedVariable
        """
        Action to select the video folder
        
        @param widget: The widget that triggered the action
        """
        self.debug("Selecting the source folder")

        editor = self.edit_source_dir

        dialog = Gtk.FileChooserDialog("Select the source folder", self, Gtk.FileChooserAction.SELECT_FOLDER,
                                       (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OPEN, Gtk.ResponseType.OK))

        current_dir = editor.get_text().strip()
        if os.path.isdir(current_dir):
            dialog.set_current_folder(current_dir)

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            editor.set_text(dialog.get_filename())
            self.debug("Source folder selected: " + dialog.get_filename())
            self.app_settings.set_app_settings(AppSettings.CFG_SOURCE_DIR, dialog.get_filename())

        dialog.destroy()

    def _do_video_convert(self, widget):  # @UnusedVariable
        """
        Action to convert the selected video files
        
        @param widget: The widget that triggered the action
        """
        codec_settings = CodecSettings(self._get_ffmpeg_features())
        names = codec_settings.get_all_codec_names() 
        codec_name = InputComboBoxDialog(self, 'Select the new file format', names, names[0]).do_show_and_get_value()
        if codec_name is not None:
            
            codec = codec_settings.get_codec_info(codec_name)
            file_suffix = codec.get_settings(CodecInfo.SUFFIX)

            params = ["-i", self._SOURCE]
            params.extend(codec.get_settings(CodecInfo.PARAMS))
            params.append(self._DESTINATION)

            file_list = self._get_selected_files_list()
            total_secs = self._get_selected_files_total_time()

            self._run_ffmpeg("Converting videos to the format" + codec_name, params, file_list, total_secs, file_suffix, None, True)

    def _do_video_concatenate(self, widget):  # @UnusedVariable
        """
        Action to concatenate the selected video files
        
        @param widget: The widget that triggered the action
        """
        app_path = ConcatenateDialog(self, self._get_selected_files_list(), self.edit_source_dir.get_text(), self._codec_settings).show_and_get_info()
        if app_path is not None:
            destination_file = app_path[ConcatenateDialog.CFG_TARGET_FILE]
            codec = app_path[ConcatenateDialog.CFG_TARGET_CODEC]

            params = ["-f", "concat", "-safe", "0", "-i", app_path[ConcatenateDialog.CFG_TEMP_FILE]]
            params.extend(codec["params"])
            params.append(self._DESTINATION)

            total_seconds = self._get_selected_files_total_time()
            self._run_ffmpeg("Concatenating videos", params, None, total_seconds, None, destination_file, True)

            # Remove the temporary file
            if os.path.isfile(app_path[ConcatenateDialog.CFG_TEMP_FILE]):
                os.remove(app_path[ConcatenateDialog.CFG_TEMP_FILE])

    def _do_video_extract_interval(self, widget):  # @UnusedVariable
        """
        Action to extract an interval from a single video file
        
        @param widget: The widget that triggered the action
        """
        # Check if it's a single file
        file_list = self._get_selected_files_list()
        if len(file_list) != 1:
            return self._show_message("You need to select only one file", "This action is only available for single files.")

        # Get the total video size
        for row in self.store:
            if row[0]:
                video_duration = row[3][10:18]

        # Get the interval
        app_path = ExtractDialog(self, "00:00:00", video_duration).show_and_get_info()
        if app_path is not None:
            ini = app_path[ExtractDialog.CFG_START]
            fim = app_path[ExtractDialog.CFG_END]

            temp = self.utils.seconds_to_time(self.utils.time_to_seconds(fim) - self.utils.time_to_seconds(ini))

            params = ["-i", self._SOURCE, "-ss", ini, "-t", temp, "-strict", "-2", self._DESTINATION]

            file_list = self._get_selected_files_list()
            total_time = self._get_selected_files_total_time()

            self._run_ffmpeg("Extract an time interval from the video file", params, file_list, total_time, "_section."+self._EXTENSION, None, True)

    def _do_video_extract_region(self, widget):  # @UnusedVariable
        """
        Action to extract a region from a single video file (crop)
        
        @param widget: The widget that triggered the action
        """
        # Check if it's a single file
        selected_file_list = self._get_selected_files_list()
        if len(selected_file_list) != 1:
            return self._show_message("You need to select only one file", "This action is only available for single files.")

        # Locate the video resolution
        width = 1280
        height = 720
        for row in self.store:
            if row[0]:
                m = re.search(self._REGEX_TIMESTAMP, row[3])
                if m is not None:
                    temp = m.group(0)
                    width = temp[:temp.find("x")]
                    height = temp[temp.find("x") + 1:]
                    break

        # Get the required region
        app_path = CropDialog(self, int(width), int(height)).show_and_get_info()
        if app_path is not None:

            params = ["-i", self._SOURCE, "-vf", "crop=" + str(app_path[CropDialog.WIDTH]) + ":" + str(app_path[CropDialog.HEIGHT]) + ":" + str(app_path[CropDialog.X]) + ":" + str(app_path[CropDialog.Y]), "-strict", "-2", self._DESTINATION]

            selected_file_list = self._get_selected_files_list()
            total_time_secs = self._get_selected_files_total_time()

            self._run_ffmpeg("Extracting the video region", params, selected_file_list, total_time_secs, "_cropped."+self._EXTENSION, None, True)

    def _do_video_deshake(self, widget):  # @UnusedVariable
        """
        Action to try to deshake a list of videos
        
        @param widget: The widget that triggered the action
        """
        app_path = DeshakeDialog(self).show_and_get_info()
        if app_path is not None:
            intensity = app_path[DeshakeDialog.SHAKINESS]
            zoom = app_path[DeshakeDialog.ZOOM]

            vectors = self.edit_source_dir.get_text() + os.sep + "transform_vectors.trf"

            file_list = self._get_selected_files_list()
            total_time = self._get_selected_files_total_time()

            # Step 1. Calculating the stabilization vectors.
            params = ["-i", self._SOURCE, "-vf", "vidstabdetect=stepsize=6:shakiness=" + str(intensity) + ":result=" + vectors, "-f", "null", "-"]
            self._run_ffmpeg("Calculating stabilization vectors", params, file_list, total_time, None , None, False)

            # Step 2. Transcoding the video with the data from Step 1 into a nice and smooth output video file.
            params = ["-i", self._SOURCE, "-vf", "vidstabtransform=input=" + vectors + ":zoom=" + str(zoom) + ":smoothing=30,unsharp=5:5:0.8:3:3:0.4", "-strict", "-2", self._DESTINATION]
            self._run_ffmpeg("Stabilizating video", params, file_list, total_time, "_stab."+self._EXTENSION , None, True)

    def _do_video_resize(self, widget):  # @UnusedVariable
        """
        Action to resize a list of videos
        
        @param widget: The widget that triggered the action
        """
        width = 1280
        height = 720

        # Locate the current resolution
        for row in self.store:
            if row[0]:
                m = re.search(self._REGEX_TIMESTAMP, row[3])
                if m is not None:
                    temp = m.group(0)
                    width = temp[:temp.find("x")]
                    height = temp[temp.find("x") + 1:]
                    break

        # Get the required resolution
        app_path = InputTextFieldDialog(self, 'Set the new video resolution', str(width) + "x" + str(height)).do_show_and_get_value()
        if app_path is not None:
            m = re.search(self._REGEX_TIMESTAMP, app_path)
            if m is None:
                return self._show_message("Invalid resolution", "You need to inform a resolution in the format 800x600.")

            width = app_path[:app_path.find("x")]
            height = app_path[app_path.find("x") + 1:]

            params = ["-i", self._SOURCE, "-vf", "scale=w=" + width + ":h=" + height + "", "-q:a", "0", "-q:v", "0", "-strict", "-2", self._DESTINATION]

            file_list = self._get_selected_files_list()
            total_time = self._get_selected_files_total_time()

            self._run_ffmpeg("Resizing video file", params, file_list, total_time, "_resized."+self._EXTENSION, None, True)
    
    def _do_video_rotate(self, widget):  # @UnusedVariable
        """
        Action to rotate a list of videos
        
        @param widget: The widget that triggered the action
        """
        # Available rotations
        options = ["90 Degree clockwise", "90 Degree counter-clockwise", "180 Degree", "90 Degree counter-clockwise with vertical flip", "90 degree clockwise with vertical flip", "Horizontal flip ", "Vertical flip "]
        filter_param = ["transpose=1", "transpose=2", "transpose=2,transpose=2", "transpose=0", "transpose=3", "hflip", "vflip"]
        
        # Get the rotation
        app_path = InputComboBoxDialog(self, 'Select the video rotation', options, options[0]).do_show_and_get_value()
        if app_path is not None:
            for idx, item in enumerate(options):
                if item == app_path:
                    params = ["-i", self._SOURCE, "-vf", filter_param[idx], "-q:a", "0", "-q:v", "0", "-strict", "-2", self._DESTINATION]

            file_list = self._get_selected_files_list()
            total_time = self._get_selected_files_total_time()

            self._run_ffmpeg("Rotating video file", params, file_list, total_time, "_rotated."+self._EXTENSION, None, True)

    def _do_load_file_list(self, widget):  # @UnusedVariable
        """
        Action to refresh the file list
        
        @param widget: The widget that triggered the action
        """
        # Open the file reader transient
        dialog_files = FileListReaderDialog(self, "Updating the file list", self.edit_source_dir.get_text())
        self._file_map = dialog_files.show_and_get_info(self._file_map)

        # Populate the file grid
        self.store.clear()
        if self._file_map is not None:
            pos_src = len(self.edit_source_dir.get_text()) + 1
            for file_name in self._file_map:
                self.store.insert(0, [
                    False ,
                    file_name[pos_src:],
                    self.utils.to_human_size(os.stat(file_name).st_size),
                    self._file_map[file_name]
                ])

        # Update the counter
        self._do_update_counters()
        self.debug("File list updated")

    def _is_video(self, file_name):
        """
        Check if the file is a video, based on the compatible extensions
        
        @param file_name: The file to be checked
        """        
        for ext in self.video_extensions:
            if file_name.lower().endswith(ext.lower()):
                return True
        return False
    
    def _do_update_counters(self):
        """
        Update the video selection counters
        """        
        number_of_sel_files = 0
        total_bytes = 0
        secs = 0

        for row in self.store:
            if row[0]:
                file_name = self.edit_source_dir.get_text() + os.sep + row[1]
                time = row[3][10:18]

                if self._is_video(file_name):
                    number_of_sel_files += 1
                    total_bytes += os.stat(file_name).st_size
                    secs = secs + self.utils.time_to_seconds(time)

        self.label_status.set_text("Selected files: " + str(number_of_sel_files) + " / " + str(len(self.store)) + " (" + self.utils.to_human_size(total_bytes) + ") - " + self.utils.seconds_to_time(secs))

        # Update the buttons
        for button in self._button_list:
            button.set_sensitive(number_of_sel_files > 0)

        self.button_extract_interval.set_sensitive(number_of_sel_files == 1)
        self.button_extract_section.set_sensitive(number_of_sel_files == 1)
        self.button_concatenate.set_sensitive(number_of_sel_files > 1)

    def _get_selected_files_list(self):
        """
        Get the list of selected files
        """ 
        selected_files = []
        for row in self.store:
            if row[0]:
                selected_files.append(self.edit_source_dir.get_text() + os.sep + row[1])
        return selected_files

    def _get_selected_files_total_time(self):
        """
        Get the sum of seconds from the selected files
        """ 
        seconds = 0
        for row in self.store:
            if row[0]:
                seconds += self.utils.time_to_seconds(row[3][10:18])

        return seconds

    def _on_close(self, widget):  # @UnusedVariable
        """
        Close the application and release the logger
        
        @param widget: The widget that triggered the action
        """
        self._do_save_screen_size()
        sys.exit()
    
    def _run_ffmpeg(self, action_title, params, file_list, total_seconds , file_suffix, destination_file, show_completed_message):
        """
        Run the ffmpeg application
        """

        # Store the STDOUT, in case ffmpeg is interrupted
        saved_stdout = sys.stdout

        # Process the files
        dialog_video = VideoProgressDialog(self, file_list, self.edit_source_dir.get_text(), action_title, params, total_seconds)
        dialog_video.set_file_suffix(file_suffix)
        dialog_video.set_destination_file(destination_file) 
        dialog_video.process_files()

        # Interrupt the process if the user clicked stop
        dialog_video.must_stop = True
        if dialog_video.failed:
            dialog_video.destroy()
            sys.stdout = saved_stdout
            return self._show_message("Failed to process the files", "The user has requested to stop the operation.")

        dialog_video.interrrupt_process()
        dialog_video.destroy()
        self.debug("Video processing has finished")

        # Restore the original STDOUT 
        sys.stdout = saved_stdout

        if show_completed_message:
            self._show_message("Finished", "The file processing has finished successfully!")
            self._do_load_file_list(None)

    def _do_click_logs(self, widget):  # @UnusedVariable
        """
        Action to view the log
        """
        self.debug("Showing the logs")
        
        text_view = TextViewerDialog(self)
        log_file = dirname(dirname(dirname(abspath(__file__)))) + os.sep + "application.log"
        if os.path.isfile(log_file):
            f = open(log_file, "r")
            text_view.set_text(f.read())
            
        text_view.do_show_window()

    def _on_tree_double_clicked(self, widget, row, col):  # @UnusedVariable
        """
        Action triggered when double click on the file list 
        """
        self.debug("Double click on the file list (" + str(row) + "," + str(col.get_sort_column_id()) + ")")
        select = self.treeview.get_selection()
        model, treeiter = select.get_selected()
        self.store.set_value(treeiter, 0, not model[treeiter][0])
        self._do_update_counters()

    def _get_ffmpeg_features(self):
        """
        Get the features available on the ffmpeg
        """
        if not self._ffmpeg_features:
    
            app = self.get_app_settings(AppSettings.CFG_PATH_TO_FFMPEG)
            app = app if app is not None else "ffmpeg"
            ffmpeg_process = subprocess.Popen([app], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, bufsize=1, universal_newlines=True)
    
            lines = ""
            for line in iter(ffmpeg_process.stdout.readline, ''):
                if "--" in line:
                    lines = lines + line
    
            ffmpeg_process.stdout.close()
            ffmpeg_process.wait()
    
            self._ffmpeg_features = []
            pattern = re.compile(self._REGEX_FEATURES_FFMPEG)
            for m in pattern.finditer(lines):
                self._ffmpeg_features.append(m.group())
    
        return self._ffmpeg_features
    
    def _do_save_screen_size(self):
        '''
        Save the current screen position and size
        '''
        w , h = self.get_size()
        x , y = self.get_position()

        self.app_settings.set_app_settings(AppSettings.WINDOW_X, x)
        self.app_settings.set_app_settings(AppSettings.WINDOW_Y, y)
        self.app_settings.set_app_settings(AppSettings.WINDOW_W, w)
        self.app_settings.set_app_settings(AppSettings.WINDOW_H, h)
