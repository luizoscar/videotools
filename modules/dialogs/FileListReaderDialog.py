# -*- coding: utf-8 -*-

'''
###############################################################################################'
 FileListReaderDialog.py

 This is a free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This file is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with the software; see the file COPYING. If not, write to the
 Free Software Foundation, Inc.,
 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.

###############################################################################################'
 @class Dialog used to show the video processing progress. 
 @author: Luiz Oscar Machado Barbosa - <luizoscar@gmail.com>
##############################################################################################
'''
from __future__ import division
import os
import re
from threading import Thread
import subprocess
from glob import glob

from gi.repository import Gtk, GLib

from modules.dialogs.BaseContainer import BaseDialog
from modules.settings.AppSettings import AppSettings
from modules.utils.Utils import Utils


class FileListReaderDialog(BaseDialog):

    def __init__(self, parent, title, video_directory):
        """
        Dialog used to read the file list and show a progress bar
        
        @param parent: The parent window
        @param title: The dialog title
        @param video_directory: The directory to be searched 
        """
        super(FileListReaderDialog, self).__init__(title=title, transient_for=parent)
        self.add_button(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL)
        
        self.set_size_request(250, 150)
        self.set_border_width(10)

        self.app_settings = AppSettings()
        self.utils = Utils()    
        self.video_directory = video_directory
        self.video_extensions = self.app_settings.get_app_settings(AppSettings.CFG_VIDEO_EXTENSIONS).split('|');    
        self.ffmpeg_path = self.app_settings.get_app_settings(AppSettings.CFG_PATH_TO_FFMPEG)
        self.ffmpeg_path = self.ffmpeg_path if self.ffmpeg_path is not None else "ffmpeg"
        
        # Main container
        grid = Gtk.Grid()
        grid.set_column_homogeneous(True)
        grid.set_row_homogeneous(True)
        grid.set_column_spacing(4)
        grid.set_row_spacing(6)

        # Activity label
        grid.attach(Gtk.Label(label="Searching video files from " + video_directory, halign=Gtk.Align.START), 0, 0, 6, 1)

        # Progress bar
        self.progress_bar_total = Gtk.ProgressBar(show_text=True)
        grid.attach(self.progress_bar_total, 0, 1, 6, 1)

        # Current file
        self.label_total_progress = Gtk.Label(halign=Gtk.Align.START)
        grid.attach(self.label_total_progress, 0, 2, 6, 1)

        self.get_content_area().pack_start(grid, True, True, 0)
        self.show_all()
        
        # File locator thread
        thread = Thread(target=self._locate_videos)
        thread.daemon = True
        thread.start()
    
    def show_and_get_info(self,file_map):
        """
        Show the dialog and return the file list
        """
        self._previous_file_map = file_map if file_map is not None else {}
        self.run()         
        self.do_close_screen()
        return self._detail_map
    
    def _locate_videos(self):
        """
        Locate the video files and check his resolution
        Note: Used by the Thread on the __init__
        """
        # The detail map will hold the file and the resolution information
        self._detail_map = {}

        # Build the file list
        self.source_file_list = [y for x in os.walk(self.video_directory) for y in glob(os.path.join(x[0], '*.*'))]
        for file_name in self.source_file_list:
            if file_name in self._previous_file_map:
                self._detail_map[file_name] = self._previous_file_map[file_name]
        
        total_file_size = 0
        for file_name in self.source_file_list:
            total_file_size = total_file_size + os.stat(file_name).st_size  # in bytes

        self.debug("Files on the source dir: " + str(len(self.source_file_list)) + " (" + self.utils.to_human_size(total_file_size) + ")")
        self.debug("Updating the file list")
        
        
        total = len(self.source_file_list)
        current = 0
        for file_name in self.source_file_list:
            current += 1
            
            # Check if the dialog was closed
            if not self.is_visible():
                return
            
            # Get the info and update the progress bar
            if self._is_video(file_name) and os.stat(file_name).st_size > 0 and file_name not in self._detail_map:
                GLib.idle_add(self._update_progress, file_name, current, total)
                self._detail_map[file_name] = self._get_file_info(file_name)

        # close the dialog when completed
        GLib.idle_add(self.do_close_screen)

    def _update_progress(self, file_name, current, total):
        """
        Update the progress bar and current file label
        NOTE: used by GLib.idle_add() above
        
        @param file_name: The current file name
        @param percent: The total read files progress
        """    
        percent = current / total
        name = os.path.basename(file_name)
        self.label_total_progress.set_text("Checking file [ "+str(current)+" / "+str(total)+" ] - " + str(name))
        self.progress_bar_total.set_fraction(percent)  # Must be between 0.0 and 1.0

    def _is_video(self, file_name):
        """
        Check if the file is a video, based on the compatible extensions
        
        @param file_name: The file name
        """        
        for ext in self.video_extensions:
            if file_name.lower().endswith(ext.lower()):
                return True
        return False
    
    def _get_file_info(self, file_name):
        """
        Extract the video file information
        
        @param file_name: The file that will be checked
        """
        pattern = re.compile("(Duration: [0-9]{2,}:[0-9]{2,}:[0-9]{2,})|(Video: [^\s]+)|([0-9]{2,}x[0-9]{2,})|([0-9|.]+ fps)|(Audio: [^\s]+)|([0-9]+ Hz)")

        args = [self.ffmpeg_path, "-hide_banner", "-i", file_name]

        self._ffmpeg_process = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, bufsize=1, universal_newlines=True)

        lines = ""
        self.debug("------------------------------------------------------------------------------------")
        self.debug("Executing: " + " ".join(args))
        self.debug(" ")
            
        # Run the process and concatenate all lines
        for line in iter(self._ffmpeg_process.stdout.readline, ''):
            self.debug(line.strip())
            # Ignore garbage lines
            if line.find("Stream #0") or line.find(" Duration:"):
                lines = lines + line

        # Get the Regex groups
        resp = ""
        for m in pattern.finditer(lines):
            resp = resp + m.group() + " "

        # Terminate the ffmpeg process
        self._ffmpeg_process.stdout.close()
        if self._ffmpeg_process.wait() > 1:
            self.debug("Failed to execute ffmpeg, please check the application log.")

        return resp                        
        
