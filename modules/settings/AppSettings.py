# -*- coding: utf-8 -*-
'''
###############################################################################################'
 AppSettings.py

 This is a free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This file is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with the software; see the file COPYING. If not, write to the
 Free Software Foundation, Inc.,
 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.

###############################################################################################'
 @class Module that saves and retrieve the application settings
 @author: Luiz Oscar Machado Barbosa - <luizoscar@gmail.com>
##############################################################################################
'''

import os

from modules.settings.BaseXmlManager import BaseXmlManager


class AppSettings(BaseXmlManager):
    '''
    Module to set/get the system settings. 
    '''

    # Tags e atributos do XML de configuração.
    TAG_CONFIG = 'config'
    WINDOW_X = 'window_x'
    WINDOW_Y = 'window_y'
    WINDOW_H = 'window_h'
    WINDOW_W = 'window_w'
    CFG_DEBUG_MODE = 'debug_mode'
    CFG_SOURCE_DIR = 'source_dir'
    CFG_VIDEO_EXTENSIONS = 'video_extensions'
    CFG_PATH_TO_FFMPEG = 'path_to_ffmpeg'

    # Application dir
    _APP_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
    
    def __init__(self):
        '''
        Default constructor
        '''
        super(AppSettings, self).__init__(self._APP_DIR + os.sep + "settings.xml")
        # Create the settings with the default values
        if not os.path.isfile(self.xml_file):
            self.set_app_settings(self.CFG_SOURCE_DIR, os.path.expanduser('~'))
            self.set_app_settings(self.CFG_VIDEO_EXTENSIONS, "wmv|avi|mpg|3gp|mov|m4v|mts|mp4|webm|mkv")
            self.set_app_settings(self.CFG_PATH_TO_FFMPEG, "ffmpeg")
    
    def set_app_settings(self, name, value):
        """
        Set an application settings
        
        @param name: The settings name
        @param value: The settings value
        """
        root_node = self.get_root_node() if os.path.isfile(self.xml_file) else self.create_element(AppSettings.TAG_CONFIG)
    
        # If not null, create a new node
        if value is not None and str(value).strip():
            self.create_unique_element(name, root_node).text = self.unicode(value)
        else:
            root_node = self.remove_element(name)
    
        self.save(root_node)
    
    def get_app_settings(self, xml_tag):
        """
        Get an application settings
        
        @param xml_tag: The XML attribute name
        """        
        node_path = self.get_element_list("./" + xml_tag)
        return None if not node_path else self.unicode(node_path[0].text).strip()

    def set_debug_mode(self, debug_mode):
        '''
        Set the application debug mode
        
        @param debug_mode: The debug mode
        '''
        self.set_app_settings(AppSettings.CFG_DEBUG_MODE, str(debug_mode))
    
    def is_debug_mode(self):
        '''
        Check if the application is in debug mode
        '''
        return 'True' == self.get_app_settings(AppSettings.CFG_DEBUG_MODE)
