#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
###############################################################################################'
 videotools.py

 This is a free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This file is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with the software; see the file COPYING. If not, write to the
 Free Software Foundation, Inc.,
 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.

###############################################################################################'
 @class Application entry point 
 @author: Luiz Oscar Machado Barbosa - <luizoscar@gmail.com>
##############################################################################################
'''
from __future__ import absolute_import, division, print_function, unicode_literals

from distutils import spawn
import getopt
import os
import sys
import logging

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Gdk', '3.0')

from gi.repository import Gtk, GObject

from modules.dialogs.InputTextFieldDialog import InputTextFieldDialog
from modules.dialogs.MainWindow import MainWindow
from modules.settings.AppSettings import AppSettings

   
# Remove the existing log file
log_file = os.path.dirname(os.path.realpath(__file__)) + os.sep + "application.log"
if os.path.isfile(log_file):
    os.remove(log_file)

fs = '%(asctime)s %(message)s'
fmt = logging.Formatter(fs, None)
# Log to file
logging.basicConfig(level=logging.INFO, format=fs, filename=log_file, filemode='w')

# Add a log handler to display to stderr
root_logger = logging.getLogger()
hdlr = logging.StreamHandler(sys.stderr)
hdlr.setFormatter(fmt)
root_logger.addHandler(hdlr)


# Parse the application parameters
try:
    opts, args = getopt.getopt(sys.argv[1:], "h", [])
except getopt.GetoptError:
    print('videotools.py -h (help)')
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        print("\nApplication for video editing")
        print("\nUso: videotools.py -h (help)")
        print("\nEx: ./videotools.py -d\"\n")
        sys.exit()

# Force UTF-8 on Python 2.x
if sys.version_info < (3, 0):
    reload(sys)
    sys.setdefaultencoding("utf-8")

# Load the application settings
settings = AppSettings()

main_window = None
# Check the existing ffmpeg instance
app = settings.get_app_settings(AppSettings.CFG_PATH_TO_FFMPEG)
app = app if app is not None else "ffmpeg"

if not spawn.find_executable(app):
    app_path = InputTextFieldDialog(main_window, 'Please inform the path to the ffmpeg application', None).do_show_and_get_value()
    if app_path is None or not spawn.find_executable(app_path):
        print("Unable to locate the required ffmpeg application.")
        print("Please check the path to the ffmpeg executable file")
        print("Current location is: " + app)
        sys.exit(2)
    else:
        settings.set_app_settings(AppSettings.CFG_PATH_TO_FFMPEG, app_path)

# Calling GObject.threads_init() is not needed for PyGObject 3.10.2+
GObject.threads_init()

# Show de main window 
main_window = MainWindow()
main_window.show_all()
Gtk.main()
